# sandbox

## Simple Packet Parser

The Simple Packet Parser is intended to find instances of a packet with:
* One Sync Byte (0x7F)
* One Length Byte that describes how many bytes of data follow

## Building
* mkdir build
* cd build
* cmake ..
* make

## Products
* demoApp - Just instantiates the packet parser. If you have a binary file called testData/blah.bin, you could run:
    * ./demoApp testData/blah.bin
* test_simplePacketParser - Runs unit tests, using Catch2
    * More information about Catch2 at https://github.com/catchorg/Catch2

