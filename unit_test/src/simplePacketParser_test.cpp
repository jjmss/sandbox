#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "simplePacketParser.h"

TEST_CASE("Catch Test", "[simplePacketParser]")
{
    printf("Hello!\n");
}

TEST_CASE("SimplePacketParser Test", "[simplePacketParser]")
{
    uint8_t testBuffer[] = {0x7F, 0x02, 0xBE, 0xEF, 0x7F, 0x00, 0x7F, 0x05, 0x01, 0x02, 0x03, 0x04, 0x05};
    size_t testBufferSize = sizeof(testBuffer);
    SimplePacketParser thisParser;
    REQUIRE(3 == thisParser.packetFinder(&testBuffer[0], sizeof(testBuffer)));
}