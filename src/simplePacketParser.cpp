#include "simplePacketParser.h"
/*
    7F-Delimeted Format Parser Specification
 
    Input : Array of bytes (as though a stream of bytes from a serial port)
    In the byte array are a series of packets. Each packet starts with a sync byte
    of 0x7F, followed by a byte with the length, followed by (length) bytes of data. 
 
    Output: Call sendPacket() for each complete packet found and return a count of packets received
*/

SimplePacketParser::SimplePacketParser()
{

}

SimplePacketParser::~SimplePacketParser()
{
    
}

/* function sendPacket
 *
 * @descr: Prints the contents of intput
 * @param intput: an array of bytes containing 0 or more packets
 * @param input_len: the number of bytes pointed to by input
 */
int SimplePacketParser::sendPacket(uint8_t* intput, size_t input_len) //this gets called by packetFinder()
{
    printf("Note: Default sendPacket() called!\n");

    printf("(%lu bytes) Printing packet: ", input_len);

    for (size_t i = 0; i < input_len; i++)
    {
        printf("%02x ", intput[i]);
    }

    printf("\n");
    return 0;
}

/* function packetFinder
 *
 * @descr: looks for complete, valid packets in the 7F-delimeted format
 *         and calls sendPacket() for each one, then returns the number
 *         of valid packets.
 * @param input: an array of bytes containing 0 or more packets
 * @param input_len: the number of bytes pointed to by input
 */
int SimplePacketParser::packetFinder(uint8_t* input, size_t input_len)
{
    // Quick Question: Which assumptions were made when writing this function?
    //
    int pktsFound = 0;
    for (size_t i = 0; i < input_len; i++)
    {
        // Check for sync byte!
        // If we have it, make sure we have enough to get the length byte
        //
        if ((input[i] == PKT_SYNC_BYTE) && (i != input_len -1))
        {
            uint8_t pktLen = input[i+1];

            // Is the length valid?
            //
            if (input_len - i > pktLen)
            {
                sendPacket(&input[i], pktLen+2);
                i += pktLen + 1;
                pktsFound++;
            }
        }
    }

    printf("Done reading input, %d packets found!\n", pktsFound);
    return pktsFound;
}
