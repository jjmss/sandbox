#include "simplePacketParser.h"

int main(int argc, char *argv[])
{
    printf("Hello!\n");
    if (argc > 1)
    {
        FILE * thisFile = fopen(argv[1], "rb");
        uint8_t testBuffer[100] = {0};

        if (thisFile != NULL)
        {
            size_t bytesRead = fread(&testBuffer[0], 1, sizeof(testBuffer), thisFile);
            if (bytesRead > 0)
            {
                printf("Read %lu bytes from file!\n", bytesRead);

                SimplePacketParser thisParser;
                thisParser.packetFinder(&testBuffer[0], bytesRead);
            }
        }
        fclose(thisFile);
    }
    else
    {
        printf("usage: demoApp fileName\n");
    }
    return 0;
}