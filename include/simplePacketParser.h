#include <cstdio>
#include <stdint.h>

#define PKT_SYNC_BYTE 0x7F

class SimplePacketParser {
    public:
        SimplePacketParser();
        ~SimplePacketParser();
        int packetFinder(uint8_t* input, size_t input_len);
        virtual int sendPacket(uint8_t* intput, size_t input_len);
    protected:
    private:
};